import random, math, time, thread, json, os, difflib
from flask import Flask, request, jsonify, send_from_directory
import requests as req
from Queue import Queue
app = Flask(__name__)

import config
import utils

timon_url = config.timon_url
server_url = config.server_url
site_prefix = config.site_prefix
job_id = config.job_id
debug = config.debug 
port = config.port 
log_file = config.log_file
store_file = config.store_file
batch_size = config.batch_size
min_size = config.min_size
discard_path = config.discard_path
reserve_path = config.reserve_path
data_path = config.data_path
static_path = config.static_path
num_images = config.num_images

create_api = "/api/jobs/"+str(job_id)+"/entities"
detail_api = "/api/entities/"
#static_url = "/image"
#static_url = "/home/pengzhen/foodlgcs1/static"
static_url =  "/Users/apple/workspaces/autotrain/foodlg-cs/static"

callback_queue = Queue(maxsize=0)

def success(data=""):
    res = dict(
        result="success",
        data=data
            )
    return jsonify(res) 

def failure(message):
    res = dict(
        result="failure",
        message=message
            )
    return jsonify(res) 


class Entity:
    def __init__(self):
        self.id=0
        self.info=dict()
        self.job=job_id
        return
    
    def add_info(self,key,value):
        self.info[key]=value
        return
    
    def to_json(self):
        return json.dumps(dict(
                            id=self.id,
                            job=job_id,
                            info=json.dumps(self.info)
                           ))

def discard_image(result):
    '''
    move image into discard folder
    '''
    print "discard %s/%s" % (result["label"],result["name"])
    if not os.path.exists(os.path.join(discard_path,result["label"])):
        os.mkdir(os.path.join(discard_path,result["label"]))

    os.rename(os.path.join(data_path,result["label"],result["name"]),os.path.join(discard_path,result["label"],result["name"]))

    f=open(os.path.join(discard_path,result["label"],result["name"]+".log"),'w')
    f.write(json.dumps(result['log']))
    f.flush()
    f.close()
    return

def reserve_image(result):
    '''
    move image into reserve folder
    '''
    print "reserve %s/%s" % (result["label"],result["name"])
    if not os.path.exists(os.path.join(reserve_path,result["label"])):
        os.mkdir(os.path.join(reserve_path,result["label"]))

    os.rename(os.path.join(data_path,result["label"],result["name"]),os.path.join(reserve_path,result["label"],result["name"]))

    f=open(os.path.join(reserve_path,result["label"],result["name"]+".log"),'w')
    f.write(json.dumps(result['log']))
    f.flush()
    f.close()
    return

def handle_callback():
    while(True):
        try:
            request_type,entity_id=callback_queue.get()
            if request_type=="MORE_ENTITY":
	        print "hello world"
                questions = load_data()
                questions=generate_questions(questions)
                create_entities(questions)
                save_data(questions)

            elif request_type == "ENTITY_FINISHED":
		print "entity-finshed"
                try:
                    r = req.get(timon_url+detail_api+str(entity_id))
                    if r.ok:
                        entity = r.json()
                    else:
                        print "Get entity %s failed!" % str(entity_id)
                except:
                    print "Get entity %s failed!" % str(entity_id)
               
                answers = json.loads(entity['answers'])
                info = json.loads(entity['info'])
                question = info['question']
                image_results={}
                for answer in answers:
                    user = answer['user']
                    for image_result in answer['value']: 
                        if image_results.has_key(image_result['pk']):
                            result=image_results[image_result['pk']]
                        else:
                            result=dict(
                                    label=question['label'],
                                    name=image_result['name'],
                                    count_reserve=0,
                                    count_discard=0,
                                    log=[]
                                        ) 
                            image_results[image_result['pk']]=result

                        v = image_result["value"]
                        if v == 'reserve':
                            result['count_reserve']+=1
                        if v == 'discard':
                            result['count_discard']+=1
                        result['log'].append(dict(user=user,value=v,time=answer['time']))

                for result in image_results.values():
                    if result['count_reserve']>result['count_discard']:
                        reserve_image(result) 
                    else:
                        discard_image(result) 

                try:
                    r = req.delete(timon_url+detail_api+str(entity_id))
                    if r.ok:
                        print r.text
                    else:
                        print "delete entity %s failed!" % str(entity_id)
                except:
                    print "delete entity %s failed!" % str(entity_id)

                questions = load_data()
                for q in questions:
                    print q['id']
                    print entity_id
                    if int(q['id']) == int(entity_id):
                        print "remove %s" % q['id']
                        questions.remove(q)       
                        break

                #generate more entities
                if len(questions)<min_size:
                    questions=generate_questions(questions)
                    create_entities(questions)
                save_data(questions)
                #done this callback
            else:
                print "unkown request type: %s " % request_type
            callback_queue.task_done()
            time.sleep(0.1)
        except:
            print "error occur"
    return

@app.route(site_prefix+'/callback')
def callback():
    print "callback"
    entity_id = request.args.get("id",0)
    request_type = request.args.get("type","ENTITY_FINISHED") # "MORE_ENTITY"
    callback_queue.put((request_type,entity_id))
    return success() 

#@app.route(site_prefix+'/image/<path:path>')
@app.route(site_prefix+'/Users/apple/workspaces/autotrain/foodlg-cs/static/<path:path>')
def get_image(path=''):
    print path 
    return send_from_directory(static_path,path,mimetype='image/jpeg')

def create_entities(questions):
    for question in questions:
        if question.has_key("id"):
           continue
        else:
           create_entity(question)
    return

def create_entity(question):
    entity = Entity()
    entity.add_info("question",question)
    headers = {'Accept': 'application/json','Content-Type':'application/json;charset=UTF-8'}
    r = req.post(timon_url+create_api,data=entity.to_json(),headers=headers)
    if r.ok:
        result= r.json()
        question["id"]=result["id"]
    else:
        print r.text
    return

def load_data():
    if os.path.exists(store_file):
        data_str = open(store_file,"rb").read()
        if not data_str:
            data_str= "[]"
    else:
        data_str = "[]"
    data = json.loads(data_str)
    return data

def save_data(data):
    f = open(store_file,"wb") 
    data_str = json.dumps(data)
    f.write(data_str)
    f.flush()
    f.close()
    return

def log(data):
    f = open(log_file,"ab")
    f.write(data)
    f.flush()
    f.close()
    return

def get_image_info(label,image_name):
    return dict(
               pk=label+"_"+image_name,
               name=image_name,
               url=server_url+static_url+"/"+label+"/"+image_name
               )
    
def generate_questions(old_questions):
    '''
    scan all images, exclude old image, sort by image number, 
    select top batch_size images, get similar n images, generate an entity 
    '''

    questions=[]

    #get old image name list
    old_image_pk_list=[] 
    old_image_pk_dict={}
    for question in old_questions:
        for image in question['images']:
            old_image_pk_list.append(image["pk"])        
	    old_image_pk_dict[image["pk"]]=True

    is_finish=False
    labels = os.listdir(data_path)
    random.shuffle(labels)
    for label in labels:
        if is_finish:
            break
        image_list = os.listdir(os.path.join(data_path,label))
        new_image_list = []
        for image_name in image_list:
            image=get_image_info(label,image_name)
            #if image['pk'] in old_image_pk_list:
            #    continue 
    	    if old_image_pk_dict.get(image['pk'],False):
    		    continue
            new_image_list.append(image_name)

        num_batch = len(new_image_list)/num_images
        if not len(new_image_list) % num_images == 0: 
            num_batch += 1

        for idx in range(num_batch):
            batch_list = new_image_list[idx*num_images:(idx+1)*num_images]
            images=[]
            for image_name in batch_list:
                image=get_image_info(label,image_name)
                images.append(image)

            question = dict(
                   label = label,
                   images = images 
                  )
            questions.append(question)
            if len(questions) == batch_size:
                is_finish=True
                break

    #no more images
    if len(questions)==0:
        print "no more questions"
        return old_questions

    questions=old_questions+questions
    return questions 

def init():
    questions = load_data()
    if len(questions)<min_size:
        questions=generate_questions(questions)
    create_entities(questions)
    save_data(questions)
    return


   
if __name__ == "__main__":
    thread.start_new_thread( init, ())
    thread.start_new_thread( handle_callback,())
    app.debug = debug 
    app.run(host='0.0.0.0', port= port)
    #app.run(host='172.26.187.243', port= port)
