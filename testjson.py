import os
import json

store_file = os.path.join(os.getcwd(),'store.json')

def load_data():
    if os.path.exists(store_file):
        data_str = open(store_file,"rb").read()
        if not data_str:
            data_str = "[]"
    else:
        data_str = "[]"
    data = json.loads(data_str)
    return data

print load_data()
