# -*- coding:utf-8 -*-
import os,datetime
from mysqldb import mysqlhelper

sqlhelper = mysqlhelper()
sqlhelper.select_db("test")

def reserve_image(result):
    '''
    move image into reserve folder
    '''
    print "reserve %s/%s" % (result["label"],result["name"])
    if not os.path.exists(os.path.join(reserve_path,result["label"])):
        os.mkdir(os.path.join(reserve_path,result["label"]))

    os.rename(os.path.join(data_path,result["label"],result["name"]),os.path.join(reserve_path,result["label"],result["name"]))

    f=open(os.path.join(reserve_path,result["label"],result["name"]+".log"),'w')
    f.write(json.dumps(result['log']))
    f.flush()
    f.close()
    return

def discard_image(result):
    '''
    move image into discard folder
    '''
    print "discard %s/%s" % (result["label"],result["name"])
    if not os.path.exists(os.path.join(discard_path,result["label"])):
        os.mkdir(os.path.join(discard_path,result["label"]))

    os.rename(os.path.join(data_path,result["label"],result["name"]),os.path.join(discard_path,result["label"],result["name"]))

    f=open(os.path.join(discard_path,result["label"],result["name"]+".log"),'w')
    f.write(json.dumps(result['log']))
    f.flush()
    f.close()
    return

sql_select = 'select distinct image_id,food_name from timon_images'
#sql_select = 'select distinct user from timon_images'
result = sqlhelper.select(sql_select)
#print result
user_label_list = []
for (image_id,food_name) in result:
    sql_select = 'select user,label from timon_images where image_id="%s" and food_name="%s"'%(image_id,food_name)
    user_label_tuple = sqlhelper.select(sql_select)
    user_label_list.append(list(user_label_tuple))
print user_label_list
# for item in user_label_list:
#     if item[0][0] == 'admin':
#         continue
#     else:

sqlhelper.close()

