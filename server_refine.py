import random, math, time, thread, json, os, difflib, datetime
from flask import Flask, request, jsonify, send_from_directory
import requests as req
from Queue import Queue
from mysqldb import mysqlhelper

sqlhelper = mysqlhelper()
sqlhelper.select_db("test")
app = Flask(__name__)


import config
import utils

timon_url = config.timon_url
server_url = config.server_url
site_prefix = config.site_prefix
job_id = config.job_id
debug = config.debug 
port = config.port 
log_file = config.log_file
store_file = config.store_file
batch_size = config.batch_size
min_size = config.min_size
discard_path = config.discard_path
reserve_path = config.reserve_path
data_path = config.data_path
static_path = config.static_path
num_images = config.num_images

create_api = "/api/jobs/"+str(job_id)+"/entities"
detail_api = "/api/entities/"
#static_url = "/image"
#static_url = "/home/pengzhen/foodlgcs1/static"
static_url =  "/Users/apple/workspaces/autotrain/foodlg-cs/static"

callback_queue = Queue(maxsize=0)

def success(data=""):
    res = dict(
        result="success",
        data=data
            )
    return jsonify(res) 

def failure(message):
    res = dict(
        result="failure",
        message=message
            )
    return jsonify(res) 


class Entity:
    def __init__(self):
        self.id=0
        self.info=dict()
        self.job=job_id
        return
    
    def add_info(self,key,value):
        self.info[key]=value
        return
    
    def to_json(self):
        return json.dumps(dict(
                            id=self.id,
                            job=job_id,
                            info=json.dumps(self.info)
                           ))

def discard_image(result):
    '''
    move image into discard folder
    '''
    print "discard %s/%s" % (result["label"],result["name"])
    if not os.path.exists(os.path.join(discard_path,result["label"])):
        os.mkdir(os.path.join(discard_path,result["label"]))

    os.rename(os.path.join(data_path,result["label"],result["name"]),os.path.join(discard_path,result["label"],result["name"]))

    f=open(os.path.join(discard_path,result["label"],result["name"]+".log"),'w')
    f.write(json.dumps(result['log']))
    f.flush()
    f.close()
    return

def reserve_image(result):
    '''
    move image into reserve folder
    '''
    print "reserve %s/%s" % (result["label"],result["name"])
    if not os.path.exists(os.path.join(reserve_path,result["label"])):
        os.mkdir(os.path.join(reserve_path,result["label"]))

    os.rename(os.path.join(data_path,result["label"],result["name"]),os.path.join(reserve_path,result["label"],result["name"]))

    f=open(os.path.join(reserve_path,result["label"],result["name"]+".log"),'w')
    f.write(json.dumps(result['log']))
    f.flush()
    f.close()
    return

def handle_callback():
    while(True):
        try:
            request_type,entity_id=callback_queue.get()
            if request_type=="MORE_ENTITY":
	            print "hello world"
                questions = load_data()
                # questions=generate_questions(questions)
                create_entities(questions)
                # save_data(questions)

            elif request_type == "ENTITY_FINISHED":
		        print "entity-finshed"
                try:
                    r = req.get(timon_url+detail_api+str(entity_id))
                    if r.ok:
                        entity = r.json()
                    else:
                        print "Get entity %s failed!" % str(entity_id)
                except:
                    print "Get entity %s failed!" % str(entity_id)
               
                answers = json.loads(entity['answers'])
                info = json.loads(entity['info'])
                question = info['question']
                image_results={}
                for answer in answers:
                    user = answer['user']
                    for image_result in answer['value']: 
                        if image_results.has_key(image_result['pk']):
                            result=image_results[image_result['pk']]
                        else:
                            result=dict(
                                    label=question['label'],
                                    name=image_result['name'],
                                    count_reserve=0,
                                    count_discard=0,
                                    log=[]
                                        ) 
                            image_results[image_result['pk']]=result

                        v = image_result["value"]
                        label_temp = 0
                        if v == 'reserve':
                            result['count_reserve']+=1
                            label_temp = 1
                        if v == 'discard':
                            result['count_discard']+=1
                        result['log'].append(dict(user=user,value=v,time=answer['time']))
                        #added by pzl
                        image_id = image_result['name']
                        food_name = image_result['pk'].split('_')[0]
                        dt = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                        sql_select = 'select count(*) from timon_images where image_id="%s" and user="%s" and food_name="%s"'%(image_id,user,food_name)
                        result = int(sqlhelper.select(sql_select)[0][0])
                        if result == 0:
                            sql_insert = 'insert into timon_images(image_id,user,food_name,answer,date) values ("%s","%s","%s","%d","%s")'%(image_id,user,food_name,label_temp,dt)
                            sqlhelper.insert(sql_insert)
                            print 'testdb successful'
                        else:
                            pass

                #cancle the operation and move to export
                #for result in image_results.values():
                #    if result['count_reserve']>result['count_discard']:
                #        reserve_image(result) 
                #    else:
                #        discard_image(result) 

                try:
                    r = req.delete(timon_url+detail_api+str(entity_id))
                    if r.ok:
                        print r.text
                    else:
                        print "delete entity %s failed!" % str(entity_id)
                except:
                    print "delete entity %s failed!" % str(entity_id)

                questions = load_data()
                for q in questions:
                    print '180',q['id']
                    print '181',entity_id
                    if int(q['id']) == int(entity_id):
                        print '182',"remove %s" % q['id']
                        questions.remove(q)       
                        break

                #generate more entities
                if len(questions)<min_size:
                    # questions=generate_questions(questions)
                    create_entities(questions)
                # save_data(questions)
                #done this callback
            else:
                print "unkown request type: %s " % request_type
            callback_queue.task_done()
            time.sleep(0.1)
        except Exception as e:
            print e
            print "error occur"
    return

@app.route(site_prefix+'/callback')
def callback():
    print "callback"
    entity_id = request.args.get("id",0)
    request_type = request.args.get("type","ENTITY_FINISHED") # "MORE_ENTITY"
    callback_queue.put((request_type,entity_id))
    return success() 

#@app.route(site_prefix+'/image/<path:path>')
@app.route(site_prefix+'/Users/apple/workspaces/autotrain/foodlg-cs/static/<path:path>')
def get_image(path=''):
    print path 
    return send_from_directory(static_path,path,mimetype='image/jpeg')

# def create_entities(questions):
#     for question in questions:
#         if question.has_key("id"):
#            continue
#         else:
#            create_entity(question)
#     return

def create_eneities(questions):
    save_index = load_index()
    for index,question in enumerate(questions[index:index+num_images]):
        create_entity(question)
    save_index += index+1
    save_index(save_index)
    return

def create_entity(question):
    entity = Entity()
    entity.add_info("question",question)
    headers = {'Accept': 'application/json','Content-Type':'application/json;charset=UTF-8'}
    r = req.post(timon_url+create_api,data=entity.to_json(),headers=headers)
    if r.ok:
        result= r.json()
        question["id"]=result["id"]
    else:
        print r.text
    return

def load_data():
    if os.path.exists(store_file):
        data_str = open(store_file,"rb").read()
        if not data_str:
            data_str= "[]"
    else:
        data_str = "[]"
    data = json.loads(data_str)
    return data

def save_data(data):
    f = open(store_file,"wb") 
    data_str = json.dumps(data)
    f.write(data_str)
    f.flush()
    f.close()
    return

def save_index(index):
    f = open('index.txt',"wb")
    f.write(index)
    f.flush()
    f.close()
    return

def load_index():
    if os.path.exists('index.txt'):
        index = open('index.txt',"rb").read()
    else:
        index = 0
    return int(index)

def log(data):
    f = open(log_file,"ab")
    f.write(data)
    f.flush()
    f.close()
    return

def get_image_info(label,image_name):
    return dict(
               pk=label+"_"+image_name,
               name=image_name,
               url=server_url+static_url+"/"+label+"/"+image_name
               )
    

def generate_questions_list():
    questions = []

    labels = os.listdir(data_path)
    random.shuffle(labels)
    for label in labels:
        image_list = os.listdir(os.path.join(data_path,label))
        num_batch = len(image_list)/num_images
        if not len(image_list)/num_images == 0:
            number += 1

        for index in range(num_batch):
            batch_list = image_list[index*num_images:(index+1)*num_images]
            images = []
            for image_name in batch_list:
                image = get_image_info(label,image_name)
                images.append(image)

            question = dict(
                label = label,
                images = images
                )
            questions.append(question)

    return questions


def init():
    questions_list = generate_questions_list()
    save_data(questions_list)
    questions = load_data()
    create_entities(questions)
    return
   
if __name__ == "__main__":
    thread.start_new_thread( init, ())
    thread.start_new_thread( handle_callback,())
    app.debug = debug 
    app.run(host='0.0.0.0', port= port)
    #app.run(host='172.26.187.243', port= port)
